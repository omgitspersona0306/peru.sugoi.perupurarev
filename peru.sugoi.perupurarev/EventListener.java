package peru.sugoi.perupurarev;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.server.TabCompleteEvent;

import net.minecraft.server.v1_12_R1.NBTTagCompound;

@SuppressWarnings("deprecation")
public class EventListener implements Listener {

	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		if (e.getBlock().getType() == Material.COMMAND || e.getBlock().getType() == Material.COMMAND_CHAIN || e.getBlock().getType() == Material.COMMAND_REPEATING) {
			if (!(e.getPlayer().getItemInHand().getType() == Material.GOLD_AXE)) {
				e.getPlayer().sendMessage(Data.logo + "金のオノじゃないとこわせません！！");
				e.setCancelled(true);
			}
		}

	}

	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof ArmorStand && e.getCause() != DamageCause.ENTITY_ATTACK) {
			e.setCancelled(true);
		}else if (Data.dosayouch) {
			if (!(e.getEntity() instanceof Player) && !(e.getEntity() instanceof ArmorStand) && !(e.getEntity() instanceof Villager)) {
				e.getEntity().setCustomName("いてっ");
			}
		}
	}

	@EventHandler
	public void onDamageByEntity(EntityDamageByEntityEvent e) {
		if (Data.dosayouch) {
			if (!(e.getEntity() instanceof Player) && !(e.getEntity() instanceof ArmorStand) && !(e.getEntity() instanceof Villager)) {
				e.getEntity().setCustomName("いてっ");
			}
		}

		if (e.getEntity() instanceof ArmorStand) {
			if (e.getDamager() instanceof Player && ((Player)e.getDamager()).hasPermission("perupurarev.op")) {
				if (((Player)e.getDamager()).getItemInHand().getType() != Material.GOLD_AXE) {
					e.setCancelled(true);
					e.getDamager().sendMessage(Data.logo + "金のオノじゃないとこわせません！！");
					return;
				}else if (e.getEntity() instanceof ArmorStand) {
					if (Skytext.removeSkytext( ((ArmorStand)e.getEntity()) )) {
						e.getDamager().sendMessage(Data.logo + "Skytextをぶっこわしました！");
					}else {
						e.getEntity().remove();
					}

				}

			}else {
				e.setCancelled(true);
				return;
			}
		}

	}

	@EventHandler
	public void onShoot(EntityShootBowEvent e) {
		if (Data.doridearrow) {
			e.getProjectile().setPassenger(e.getEntity());
		}
	}


	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if (e.getPlayer().getItemInHand().getType() != Material.AIR && Data.doexecuteboundcommand) {
			net.minecraft.server.v1_12_R1.ItemStack nmsitem = CraftItemStack.asNMSCopy(e.getPlayer().getItemInHand());
			NBTTagCompound nbt = nmsitem.getTag();
			if (nbt == null) return;

			if (nbt.hasKey("CommandBound")) {
				e.getPlayer().sendMessage(Data.logo + "コマンドを実行: /" + nbt.getString("CommandBound"));
				e.getPlayer().performCommand(nbt.getString("CommandBound"));
			}
		}
	}

	@EventHandler
	public void onInteractEntity(PlayerInteractEntityEvent e) {
		if (e.getPlayer().hasPermission("perupurarev.op")) {
			if (e.getRightClicked() instanceof ArmorStand) {
				if (Skytext.doExistSkytext(e.getRightClicked())) {
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Skytext.getSkytext(e.getRightClicked()).getCommand());
				}
			}

		}

	}


	@EventHandler
	public void onTabComplete(TabCompleteEvent e) {
		List<String> completions = e.getCompletions();
		if (!(e.getSender() instanceof Player)) {
			return;
		}

		if ( !(e.getSender().hasPermission("perupura.op")) ) {

			if (completions.contains("/peru")) {
				completions.remove("/peru");
				completions.remove("/perupurarev:peru");
			}

		}else {
			String[] commands = {"arrowride", "bind", "debug", "help", "nbtdel", "nbtlist", "nbtadd", "pyon", "sayouch", "skytext", "sql"};

			try {
				if ( !(e.getBuffer().substring(0, 6).equalsIgnoreCase("/peru ")) ) {
					return;
				}
			}catch (Exception exception) {
				return;
			}

			if (e.getBuffer().length() <= 6) {
					for (String command : commands) {
						completions.add(command);
					}

			}else {
				for (String command : commands) {
					try {
						if (e.getBuffer().substring(6).equalsIgnoreCase(command.substring(0, e.getBuffer().length() - 6))) {
							completions.add(command);
						}

					}catch (Exception ex){ }

				}

			}

			for (Player player : e.getSender().getServer().getOnlinePlayers()) {
				completions.remove(player.getName());
			}

		}
	}

}

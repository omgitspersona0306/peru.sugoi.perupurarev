package peru.sugoi.perupurarev;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;

public class Skytext {

	private static HashMap<ArmorStand, Integer> skytextidmap = new HashMap<ArmorStand, Integer>();
	private static HashMap<Integer, Skytext> skytextmap = new HashMap<Integer, Skytext>();
	private static Set<Integer> idset = new HashSet<Integer>();

	private ArmorStand armorstand;
	private String command;
	private int id;
	private String text;

	public Skytext (ArmorStand armorstand, int id) {
		text = "Skytext" + id;

		Skytext.skytextidmap.put(armorstand, id);
		Skytext.skytextmap.put(id, this);
		Skytext.idset.add(id);

		this.armorstand = armorstand;

		armorstand.setCustomName(text);
		armorstand.setCustomNameVisible(true);
		armorstand.setVisible(false);

	}

	public Skytext (ArmorStand armorstand) {
		for (int i = 1; true; i++) {
			if (!skytextmap.containsKey(i)) {
				id = i;
				break;
			}
		}
		text = "Skytext" + id;

		Skytext.skytextidmap.put(armorstand, id);
		Skytext.skytextmap.put(id, this);
		Skytext.idset.add(id);

		this.armorstand = armorstand;

		armorstand.setCustomName(text);
		armorstand.setCustomNameVisible(true);
		armorstand.setVisible(false);

	}

	public ArmorStand getArmorStand() {
		return armorstand;
	}

	public String getCommand() {
		return command;
	}

	public int getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public static ArmorStand getArmorStand(int id) {
		try {
			ArmorStand armorstand = getSkytext(id).getArmorStand();
			return armorstand;

		}catch (NullPointerException e) {
			return null;
		}

	}

	public static ArmorStand getArmorStand(Skytext skytext) {
		try {
			ArmorStand armorstand = skytext.getArmorStand();
			return armorstand;

		}catch (NullPointerException e) {
			return null;
		}

	}

	public static int getId(Entity armorstand) {
		if (armorstand instanceof ArmorStand) {
			try {
				int id = skytextidmap.get(armorstand);
				return id;
			}catch (NullPointerException e) {
				return -1;
			}

		}else {
			return -1;
		}

	}

	public static int getId(Skytext skytext) {

		try {
			int id = skytext.getId();
			return id;

		}catch (NullPointerException e) {
			return -1;
		}
	}

	public static String[] getList() {
		String[] list = new String[idset.size()];
		int i = 0;
		for (int id : idset) {
			list[i] = id + ":" + getText(id);
			i++;
		}

		return list;

	}

	public static String getText(Entity armorstand) {
		String text = null;
		if (doExistSkytext(armorstand)) {
			text = getSkytext(armorstand).text;
		}

		return text;
	}

	public static String getText(int id) {
		String text = null;
		if (doExistSkytext(id)) {
			text = getSkytext(id).text;
		}

		return text;

	}

	public static Skytext getSkytext(Entity armorstand) {
		if (armorstand instanceof ArmorStand) {
			int id = getId(armorstand);
			try {
				Skytext skytext = getSkytext(id);
				return skytext;

			}catch (NullPointerException e) {
				return null;
			}
		}else {
			return null;
		}

	}

	public static Skytext getSkytext(int id) {
		Skytext skytext = skytextmap.get(id);

		return skytext;
	}

	public static boolean doExistSkytext(Entity armorstand) {
		if (armorstand instanceof ArmorStand) {
			Skytext skytext = getSkytext(armorstand);
			if (!(Objects.isNull(skytext))) {
				return true;

			}else {
				return false;

			}

		}else {
			return false;
		}
	}

	public static boolean doExistSkytext(int id) {
		Skytext skytext = getSkytext(id);
		if (!(Objects.isNull(skytext))) {
			return true;

		}else {
			return false;

		}
	}

	public static boolean removeSkytext(Entity armorstand) {
		if (doExistSkytext(armorstand)) {
			int id = getId(armorstand);

			armorstand.remove();

			skytextmap.remove(id);
			skytextidmap.remove(armorstand);
			idset.remove(id);

			return true;

		}else {
			armorstand.remove();
			return false;

		}
	}

	public static boolean removeSkytext(int id) {
		if (doExistSkytext(id)) {
			ArmorStand armorstand = getArmorStand(id);
			armorstand.remove();

			skytextmap.remove(id);
			skytextidmap.remove(armorstand);
			idset.remove(id);

			return true;

		}else {
			return false;

		}
	}

	public static boolean removeSkytext(Skytext skytext) {
		ArmorStand armorstand = getArmorStand(skytext);
		int id = getId(skytext);

		skytextmap.remove(id);
		skytextidmap.remove(armorstand);

		return true;
	}
}

package peru.sugoi.perupurarev;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import net.minecraft.server.v1_12_R1.NBTTagCompound;

@SuppressWarnings("deprecation")
public class Commander implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String index, String[] args) {
		if (!sender.hasPermission("perupurarev.op")) {
			sender.sendMessage("Unknown command. Type \"/help\" for help.");
			return true;
		}

		if (args.length == 0) {

			help(sender);
			return true;

		}else if (args[0].equalsIgnoreCase("ar") || args[0].equalsIgnoreCase("arrow") || args[0].equalsIgnoreCase("arrowride") || args[0].equalsIgnoreCase("ridearrow")) {
			if (args.length == 1) {
				if (Data.doridearrow) {
					sender.sendMessage(Data.logo + "撃った矢に乗る設定はいま " + ChatColor.AQUA + "ON");
				}else {
					sender.sendMessage(Data.logo + "撃った矢に乗る設定はいま " + ChatColor.RED + "OFF");
				}

				sender.sendMessage(Data.logo + "/peru arrowride y/n で切り替えできます");
				sender.sendMessage(Data.logo + "arrowrideの代わりに ar、 arrow、 ridearrow も使えます");
				return true;

			}else{
				if (args[1].equalsIgnoreCase("y") || args[1].equalsIgnoreCase("on") || args[1].equalsIgnoreCase("true") || args[1].equalsIgnoreCase("enable") || args[1].equalsIgnoreCase("yes") || args[1].equalsIgnoreCase("はい")) {
					Data.doridearrow = true;
					sender.sendMessage(Data.logo + ChatColor.GREEN + "撃った矢に " + ChatColor.AQUA + "乗る" + ChatColor.GREEN + " ようになりました！！！");
					return true;

				}else if (args[1].equalsIgnoreCase("n") || args[1].equalsIgnoreCase("off") || args[1].equalsIgnoreCase("false" ) || args[1].equalsIgnoreCase("disable") || args[1].equalsIgnoreCase("no") || args[1].equalsIgnoreCase("いいえ")) {
					Data.doridearrow = false;
					sender.sendMessage(Data.logo + ChatColor.GREEN + "撃った矢に " + ChatColor.RED + "乗らない" + ChatColor.GREEN + " ようになりました！！！");
					return true;

				}else {
					sender.sendMessage(Data.logo + "撃った矢に乗る設定はいま " + Data.doridearrow);
					sender.sendMessage(Data.logo + "/peru arrowride y/n で切り替えできます");
					sender.sendMessage(Data.logo + "arrowrideの代わりに ar、 arrow、 ridearrow も使えます");
					return true;

				}

			}

		}else if (args[0].equalsIgnoreCase("bind")) {

			if (!(sender instanceof Player)) {
				help(sender);
				return true;
			}else if (args.length == 1) {
				if (Data.doexecuteboundcommand) {
					sender.sendMessage(Data.logo + "アイテムのコマンドを実行する設定はいま " + ChatColor.AQUA + "ON");
				}else {
					sender.sendMessage(Data.logo + "アイテムのコマンドを実行する設定はいま " + ChatColor.RED + "OFF");
				}

				sender.sendMessage(Data.logo + "/peru bind <command> : お手持ちのアイテムにコマンドを付与します");
				sender.sendMessage(Data.logo + "/peru bind y/n : アイテムのコマンドを実行するかどうかを設定します");
				return true;

			}else {
				if (args[1].equalsIgnoreCase("y") || args[1].equalsIgnoreCase("on") || args[1].equalsIgnoreCase("true") || args[1].equalsIgnoreCase("yes")) {
					Data.doexecuteboundcommand = true;
					sender.sendMessage(Data.logo + ChatColor.GREEN + "アイテムにつけたコマンドをクリックで実行" + ChatColor.AQUA + "する" + ChatColor.GREEN + "ようになったど");
					return true;

				}else if (args[1].equalsIgnoreCase("n") || args[1].equalsIgnoreCase("off") || args[1].equalsIgnoreCase("false") || args[1].equalsIgnoreCase("no")) {
					Data.doexecuteboundcommand = false;
					sender.sendMessage(Data.logo + ChatColor.GREEN + "アイテムにつけたコマンドをクリックで実行" + ChatColor.RED + "しない" + ChatColor.GREEN + "ようになったど");
					return true;

				}

				if (((Player)sender).getItemInHand().getType() == Material.AIR) {
					sender.sendMessage(Data.logo + ChatColor.RED + "アイテムを持ってからコマンド打ってくろ");
				}
				StringBuilder sb = new StringBuilder();

				if(args[1].startsWith("/")) args[1] = args[1].substring(1 , args[1].length());

				for(int i = 0; i < args.length - 1; i++) {
					sb.append(args[i + 1] + " ");
				}

				ItemStack itemhand = ((Player)sender).getItemInHand();
				net.minecraft.server.v1_12_R1.ItemStack nmsitem = CraftItemStack.asNMSCopy(itemhand);
				NBTTagCompound nbt = nmsitem.getTag();
				if (nbt == null) {
					nbt = new NBTTagCompound();
				}

				String set = new String(sb);
				nbt.setString("CommandBound", set);
				nmsitem.setTag(nbt);
				itemhand = CraftItemStack.asBukkitCopy(nmsitem);
				((Player)sender).setItemInHand(itemhand);

				sender.sendMessage(Data.logo + ChatColor.GREEN + "コマンドをセット: /" + set);
				return true;
			}

		}else if (args[0].equalsIgnoreCase("debug")) {

			sender.sendMessage(Data.logo + "まだ作ってないよ！！");
			return true;

		}else if (args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?") || args[0].equalsIgnoreCase("commands")) {

			try {
				help(sender, Integer.parseInt(args[1]));
			}catch (Exception e) {
				help(sender);
			}
			return true;

		}else if (args[0].equalsIgnoreCase("nbtadd") || args[0].equalsIgnoreCase("addnbt") || args[0].equalsIgnoreCase("nbtset") || args[0].equalsIgnoreCase("setnbt")) {

			if (!(sender instanceof Player)) {
				help(sender);
				return true;

			}else if (args.length < 3) {
				sender.sendMessage(Data.logo + "/peru nbtadd <Key> <Value> (<ValueType>)");
				sender.sendMessage(Data.logo + "<ValueType>を指定しない場合はString(文字列)になります");
				sender.sendMessage(Data.logo + "nbtadd ではなく addnbt、 nbtset、 setnbt でもいけます");
				return true;

			}else {
				if (((Player)sender).getItemInHand().getType() == Material.AIR) {
					sender.sendMessage(Data.logo + ChatColor.RED + "手にアイテムを持ってくんろ");
					return true;
				}

				ItemStack itemhand = ((Player)sender).getItemInHand();
				net.minecraft.server.v1_12_R1.ItemStack nmsitem = CraftItemStack.asNMSCopy(itemhand);
				NBTTagCompound nbt = nmsitem.getTag();
				if (nbt == null) nbt = new NBTTagCompound();

				if (args.length == 3) {
					nbt.setString(args[1], args[2]);
					sender.sendMessage(Data.logo + "NBTタグをセット: " + args[1] + " : " + args[2] + " : String");

				}else if (args.length >= 4) {
					if (args[3].equalsIgnoreCase("Boolean")) {
						try {
							nbt.setBoolean(args[1], Boolean.parseBoolean(args[2]));
							sender.sendMessage(Data.logo + "NBTタグをセット: " + args[1] + " : " + args[2] + " : Boolean");
						}catch(Exception e) {
							sender.sendMessage(Data.logo + ChatColor.RED + args[2] + "をBoolean型に変換できませんでした");
							return true;
						}

					}else if (args[3].equalsIgnoreCase("Byte")) {
						try {
							nbt.setByte(args[1], Byte.parseByte(args[2]));
							sender.sendMessage(Data.logo + "NBTタグをセット: " + args[1] + " : " + args[2] + " : Byte");
						}catch(Exception e) {
							sender.sendMessage(Data.logo + ChatColor.RED + args[2] + "をByte型に変換できませんでした");
							return true;
						}

					}else if (args[3].equalsIgnoreCase("ByteArray")) {
						sender.sendMessage(Data.logo + "ByteArrayまだつくってない！！");
						return true;

					}else if (args[3].equalsIgnoreCase("Double")) {
						try {
							nbt.setDouble(args[1], Double.parseDouble(args[2]));
							sender.sendMessage(Data.logo + "NBTタグをセット: " + args[1] + " : " + args[2] + " : Double");
						}catch(Exception e) {
							sender.sendMessage(Data.logo + ChatColor.RED + args[2] + "をDouble型に変換できませんでした");
							return true;
						}

					}else if (args[3].equalsIgnoreCase("Float")) {
						try {
							nbt.setFloat(args[1], Float.parseFloat(args[2]));
							sender.sendMessage(Data.logo + "NBTタグをセット: " + args[1] + " : " + args[2] + " : Float");
						}catch(Exception e) {
							sender.sendMessage(Data.logo + ChatColor.RED + args[2] + "をFloat型に変換できませんでした");
							return true;
						}

					}else if (args[3].equalsIgnoreCase("Int")) {
						try {
							nbt.setInt(args[1], Integer.parseInt(args[2]));
							sender.sendMessage(Data.logo + "NBTタグをセット: " + args[1] + " : " + args[2] + " : Int");
						}catch(Exception e) {
							sender.sendMessage(Data.logo + ChatColor.RED + args[2] + "をInt型に変換できませんでした");
							return true;
						}

					}else if (args[3].equalsIgnoreCase("IntArray")) {
						sender.sendMessage(Data.logo + "IntArrayまだつくってない！！");
						return true;

					}else if (args[3].equalsIgnoreCase("Long")) {
						try {
							nbt.setLong(args[1], Long.parseLong(args[2]));
							sender.sendMessage(Data.logo + "NBTタグをセット: " + args[1] + " : " + args[2] + " : Long");
						}catch(Exception e) {
							sender.sendMessage(Data.logo + ChatColor.RED + args[2] + "をLong型に変換できませんでした");
							return true;
						}

					}else if (args[3].equalsIgnoreCase("Short")) {
						try {
							nbt.setShort(args[1], Short.parseShort(args[2]));
							sender.sendMessage(Data.logo + "NBTタグをセット: " + args[1] + " : " + args[2] + " : Short");
						}catch(Exception e) {
							sender.sendMessage(Data.logo + ChatColor.RED + args[2] + "をShort型に変換できませんでした");
							return true;
						}

					}else if (args[3].equalsIgnoreCase("String")) {
						sender.sendMessage(Data.logo + "NBTタグをセット: " + args[1] + " : " + args[2] + " : String");
						nbt.setString(args[1], args[2]);

					}else {
						sender.sendMessage(Data.logo + "対応してるValueTypeは");
						sender.sendMessage(Data.logo + "Boolean, Byte, ByteArray, Double, Float,");
						sender.sendMessage(Data.logo + "Int, IntArray, Long, Short, String です");
						return true;
					}
				}

				nmsitem.setTag(nbt);
				itemhand = CraftItemStack.asBukkitCopy(nmsitem);
				((Player)sender).setItemInHand(itemhand);
				return true;
			}

		}else if (args[0].equalsIgnoreCase("nbtdel") || args[0].equalsIgnoreCase("delnbt") || args[0].equalsIgnoreCase("nbtremove") || args[0].equalsIgnoreCase("removenbt") || args[0].equalsIgnoreCase("deletenbt") || args[0].equalsIgnoreCase("nbtdelete")) {

			if (!(sender instanceof Player)) {
				help(sender);
				return true;

			}else if (args.length > 2) {
				sender.sendMessage(Data.logo + "/peru nbtdel <Key>");
				sender.sendMessage(Data.logo + "nbtdel ではなく nbtclear、 nbtremove でもいけます");
				return true;

			}else {
				if (((Player)sender).getItemInHand().getType() == Material.AIR) {
					sender.sendMessage(Data.logo + ChatColor.RED + "手にアイテムを持ってくんろ");
					return true;
				}

				ItemStack itemhand = ((Player)sender).getItemInHand();
				net.minecraft.server.v1_12_R1.ItemStack nmsitem = CraftItemStack.asNMSCopy(itemhand);
				NBTTagCompound nbt = nmsitem.getTag();
				if (nbt == null) {
					sender.sendMessage(Data.logo + "NBTタグを抹消: " + args[1]);
					return true;
				}

				nbt.remove(args[1]);
				nmsitem.setTag(nbt);
				itemhand = CraftItemStack.asBukkitCopy(nmsitem);
				((Player)sender).setItemInHand(itemhand);
				sender.sendMessage(Data.logo + "NBTタグを抹消: " + args[1]);
				return true;

			}

		}else if (args[0].equalsIgnoreCase("nbtlist") || args[0].equalsIgnoreCase("listnbt")) {

			if (!(sender instanceof Player)) {
				help(sender);
				return true;
			}else if (((Player)sender).getItemInHand().getType() == Material.AIR) {
				sender.sendMessage(Data.logo + ChatColor.RED + "手にアイテムを持ってくんろ");
				return true;
			}

			ItemStack itemhand = ((Player)sender).getItemInHand();
			net.minecraft.server.v1_12_R1.ItemStack nmsitem = CraftItemStack.asNMSCopy(itemhand);
			NBTTagCompound nbt = nmsitem.getTag();
			if (nbt == null) {
				sender.sendMessage(Data.logo + "このアイテムにNBTタグはありません！");
				return true;
			}

			sender.sendMessage(Data.logo + itemhand.getType() + " : " + nbt);
			return true;

		}else if (args[0].equalsIgnoreCase("pyon")) {

			if (args.length == 1) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(Data.logo + "/peru pyon <player> でプレイヤーをとばせます");
					return true;
				}

				((Player)sender).setVelocity(new Vector(((Player)sender).getVelocity().getX(), 100, ((Player)sender).getVelocity().getZ()));
				sender.sendMessage(Data.logo + "ぴょ～ん");
				sender.sendMessage(Data.logo + "Tip: /peru pyon <player> で他のプレイヤーをとばせます");
				return true;

			}else {
				try {
					Bukkit.getPlayer(args[1]).setVelocity(new Vector(Bukkit.getPlayer(args[1]).getVelocity().getX(), 100, Bukkit.getPlayer(args[1]).getVelocity().getZ()));
					Bukkit.getPlayer(args[1]).sendMessage(Data.logo + "ぴょ～ん");
				}catch (Exception e) {
					sender.sendMessage(Data.logo + ChatColor.RED + "プレイヤー " + args[1] + " は見つかりませんでした！！(T_T)");
				}

				return true;
			}

		}else if (args[0].equalsIgnoreCase("sayouch") || args[0].equalsIgnoreCase("ouch") || args[0].equalsIgnoreCase("ouchsay")) {
			if (args.length == 1) {
				if (Data.dosayouch) {
					sender.sendMessage(Data.logo + "「いてっ」と言う設定はいま " + ChatColor.AQUA + "ON");
				}else {
					sender.sendMessage(Data.logo + "「いてっ」と言う設定はいま " + ChatColor.RED + "OFF");
				}

				sender.sendMessage(Data.logo + "/peru sayouch y/n で切り替えできます");
				sender.sendMessage(Data.logo + "sayouchの代わりに ouch、 ouchsay も使えます");
				return true;

			}else{
				if (args[1].equalsIgnoreCase("y") || args[1].equalsIgnoreCase("on") || args[1].equalsIgnoreCase("true") || args[1].equalsIgnoreCase("enable") || args[1].equalsIgnoreCase("yes") || args[1].equalsIgnoreCase("はい")) {
					Data.dosayouch = true;
					sender.sendMessage(Data.logo + ChatColor.GREEN + "ダメージを受けたMOBが「いてっ」って " + ChatColor.AQUA + "言う" + ChatColor.GREEN + " ようになりました！！！");
					return true;

				}else if (args[1].equalsIgnoreCase("n") || args[1].equalsIgnoreCase("off") || args[1].equalsIgnoreCase("false") || args[1].equalsIgnoreCase("disable") || args[1].equalsIgnoreCase("no") || args[1].equalsIgnoreCase("いいえ")) {
					Data.dosayouch = false;
					sender.sendMessage(Data.logo + ChatColor.GREEN + "ダメージを受けたMOBが「いてっ」って " + ChatColor.RED + "言わない" + ChatColor.GREEN + " ようになりました！！！");
					return true;

				}else {
					if (Data.dosayouch) {
						sender.sendMessage(Data.logo + "「いてっ」と言う設定はいま " + ChatColor.AQUA + "ON");
					}else {
						sender.sendMessage(Data.logo + "「いてっ」と言う設定はいま " + ChatColor.RED + "OFF");
					}

					sender.sendMessage(Data.logo + "/peru sayouch y/n で切り替えできます");
					sender.sendMessage(Data.logo + "sayouchの代わりに ouch、 ouchsay も使えます");
					return true;

				}
			}

		}else if (args[0].equalsIgnoreCase("skytext") || args[0].equalsIgnoreCase("textsky")) {

			if (!(sender instanceof Player)) {
				help(sender);
				return true;
			}

			if (args.length == 1) {
				sender.sendMessage(Data.logo + "/peru skytext del <id>   : 空中に文字だし機を抹消します");
				sender.sendMessage(Data.logo + "/peru skytext new (<id>) : 空中に文字だし機を作成します");
				sender.sendMessage(Data.logo + "/peru skytext list       : 空中に文字だし機の一覧を見ます");

				return true;

			}else if (args[1].equalsIgnoreCase("new") || args[1].equalsIgnoreCase("create")) {
				if (args.length >= 3) {
					try {
						int id = Integer.parseInt(args[2]);
						ArmorStand armorstand = (ArmorStand) ((Player)sender).getWorld().spawnEntity(((Player)sender).getLocation(), EntityType.ARMOR_STAND);
						armorstand.setGravity(false);

						Skytext skytext = new Skytext(armorstand, id);

						sender.sendMessage(Data.logo + ChatColor.GREEN + "できたよ！ IDは " + skytext.getId() + " だよ");
						return true;

					}catch(Exception e) {
						sender.sendMessage(Data.logo + ChatColor.RED + "整数をいれてってば");
						return true;

					}
				}

				ArmorStand armorstand = (ArmorStand) ((Player)sender).getWorld().spawnEntity(((Player)sender).getLocation(), EntityType.ARMOR_STAND);
				armorstand.setGravity(false);

				Skytext skytext = new Skytext(armorstand);

				sender.sendMessage(Data.logo + ChatColor.GREEN + "できたよ！ IDは " + skytext.getId() + " だよ");
				return true;

			}else if (args[1].equalsIgnoreCase("list")) {

				String[] list = Skytext.getList();
				if (list.length == 0) {
					sender.sendMessage(Data.logo + ChatColor.RED + "Skytextない！！");
					return true;
				}


				int maxpages = list.length / 8 + 1;
				int page = 1;

				try {
					page = Integer.parseInt(args[2]);
					if (page > maxpages) {
						sender.sendMessage(Data.logo + ChatColor.RED + "ページは 1 から " + maxpages + "までしかないよ＞＜;");
						return true;
					}else if (page < 1) {
						sender.sendMessage(Data.logo + ChatColor.RED + "ページは 1 から " + maxpages + "までしかないよ＞＜;");
						return true;
					}

				}catch(Exception e) {}

				sender.sendMessage(Data.logo + "Skytext list page " + page + " of " + maxpages);
				for (int i = 0; i < 8; i++) {
					try {
						sender.sendMessage(Data.logo + list[8 * (page - 1) + i]);
					}catch (Exception e) {
						sender.sendMessage(Data.logo);
					}
				}
				sender.sendMessage(Data.logo + "/peru skytext list <page> でほかのページも見れます");
				return true;

			}else if (args[1].equalsIgnoreCase("del")) {
				int id = -1;
				try {
					id = Integer.parseInt(args[2]);

				}catch(Exception e) {
					sender.sendMessage(Data.logo + ChatColor.RED + "整数をいれてってば");
					return true;

				}

				Skytext.removeSkytext(id);
				sender.sendMessage(Data.logo + ChatColor.GREEN + "ID " + id + " を削除しましたぁ～");
				return true;


			}else {
				sender.sendMessage(Data.logo + "/peru skytext del <id>   : 空中に文字だし機を抹消します");
				sender.sendMessage(Data.logo + "/peru skytext new (<id>) : 空中に文字だし機を作成します");
				sender.sendMessage(Data.logo + "/peru skytext list       : 空中に文字だし機の一覧を見ます");

				return true;

			}

		}else if (args[0].equalsIgnoreCase("sql")) {

			sender.sendMessage(Data.logo + "いまつくってんのー");
			return true;

		}else {

			help(sender);
			return true;
		}
	}

	private void help(CommandSender sender) {
		help(sender, 1);
	}


	private void help(CommandSender sender, int page){

		String waku = ChatColor.GOLD + "-- peru.sugoi.perupurarev help ";

		if (sender instanceof Player) {
			String[] helps = {
					"arrowride : 撃った矢に乗るようにできます",
					"bind      : コマンドをらくらく実行できるアイテムを作成します",
					"debug     : デバッグ機能の説明を見ます",
					"help      : コマンドの一覧を見ます",
					"nbtdel    : お手持ちのアイテムのNBTタグを消します",
					"nbtlist   : お手持ちのアイテムのNBTタグを見ます",
					"nbtadd    : お手持ちのアイテムに好きなNBTタグをつけます",
					"pyon      : ぴょ～ん",
					"sayouch   : ダメージを受けたMOBが「いてっ」と言います",
					"skytext   : 空中に文字を出します！クリック時の処理も設定できます",
					"sql       : SQLiteを使ってデータベースを読み込みます！"
					};
			int maxpages = helps.length / 7 + 1;

			if (page > maxpages) {
				sender.sendMessage(Data.logo + ChatColor.RED + "ページは 1 から " + maxpages + "までしかないよ＞＜;");
				return;
			}else if (page < 1) {
				sender.sendMessage(Data.logo + ChatColor.RED + "ページは 1 から " + maxpages + "までしかないよ＞＜;");
				return;
			}

			sender.sendMessage(Data.logo + waku + "page " + page + " of " + maxpages);
			for (int i = 0; i < 7; i++) {
				try {
					sender.sendMessage(Data.logo + helps[7 * (page - 1) + i]);
				}catch (Exception e) {
					sender.sendMessage(Data.logo);
				}
			}
			sender.sendMessage(Data.logo + waku + "page " + page + " of " + maxpages);
			sender.sendMessage(Data.logo + "/peru ? <page> でほかのページも見れます");

		}else {
			String[] helps = {
					"arrowride : 撃った矢に乗るようにできます",
					"debug     : デバッグ機能の説明を見ます",
					"help      : コマンドの一覧を見ます",
					"pyon      : ぴょ～ん",
					"sayouch   : ダメージを受けたMOBが「いてっ」と言います",
					"sql       : SQLiteを使ってデータベースを読み込みます！"
					};
			int maxpages = helps.length / 7 + 1;

			if (page > maxpages) {
				sender.sendMessage(Data.logo + ChatColor.RED + "ページは 1 から " + maxpages + "までしかないよ＞＜;");
				return;
			}else if (page < 1) {
				sender.sendMessage(Data.logo + ChatColor.RED + "ページは 1 から " + maxpages + "までしかないよ＞＜;");
				return;
			}

			sender.sendMessage(Data.logo + waku + "page " + page + " of " + maxpages);
			for (int i = 0; i < 7; i++) {
				try {
					sender.sendMessage(Data.logo + helps[7 * (page - 1) + i]);
				}catch (Exception e) {
					sender.sendMessage(Data.logo);
				}
			}
			sender.sendMessage(Data.logo + waku + "page " + page + " of " + maxpages);
			sender.sendMessage(Data.logo + "/peru ? <page> でほかのページも見れます");

		}
	}

}

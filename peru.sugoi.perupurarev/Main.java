package peru.sugoi.perupurarev;

import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	@Override
	public void onDisable() {
		// TODO 自動生成されたメソッド・スタブ
		super.onDisable();
	}

	@Override
	public void onEnable() {
		super.onEnable();

		PluginManager pm = getServer().getPluginManager();
		Permission p = new Permission("perupurarev.op");
		pm.addPermission(p);

		pm.registerEvents(new EventListener(), this);
		getCommand("peru").setExecutor(new Commander());
	}
}
